
public class ProbableTranslations implements Comparable<ProbableTranslations>
{
	
	public float newFrequency;
	public String possibleSentences;
	
	public ProbableTranslations(float frequency, String probSentences)
	{
		possibleSentences = probSentences;
		newFrequency = frequency;
	}

	@Override
	public int compareTo(ProbableTranslations arg0) 
	{
		int thineResult = 0;
		if(this.newFrequency > arg0.newFrequency)
		{
			thineResult = 1;
		}
		else if(this.newFrequency < arg0.newFrequency)
		{
			thineResult = -1;
		}
		
		return thineResult;
	}
	
	@Override
	public String toString()
	{
		return 	possibleSentences + " Frquency: " + newFrequency;
	}

}
