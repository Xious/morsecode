import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import edu.neumont.nlp.DecodingDictionary;


public class ExhaustiveDecoder 
{
	final float THRESH_HOLD = 0.0001f;
	final float DIVIDE_FREQUENCY = 10000.0f;
	final int  TOP_PERMUTATIONS = 20;

	private DecodingDictionary myDictionary = new DecodingDictionary();
	public List<String> myTranslation = new ArrayList<String>();
	public List<ProbableTranslations> myGuessedTranslations = new ArrayList<ProbableTranslations>();


	//Actually decodes the morse code that we are trying to translate
	public List<String> decode(String theCode)
	{
		myDecryptingMachine("",theCode);
		return myTranslationBuffer();
	}

	//MOLE TO DIG THROUGH THE ALGO (MOLEY MOLEY MOLEY MOLEY)
	public void myDecryptingMachine(String thineGargledSentence, String theCodeOfMorse)
	{
		//Checks the boundary case, and if null, adds the sentence so far, if the sentence is less than thresh-hold (0.90f in this case) then it will just break from the if statement all together.
		if(checkThineCoherence(thineGargledSentence) > THRESH_HOLD || thineGargledSentence.isEmpty())
		{
			if(theCodeOfMorse.isEmpty())
			{
				myGuessedTranslations.add(new ProbableTranslations(checkThineCoherence(thineGargledSentence), thineGargledSentence));
			}
			//End of above if statement for checking Boundaries.

			else
			{
				int thineIndex = 0;
				int thineJourneysEnd = theCodeOfMorse.length();

				//Goes through list
				while(thineIndex <= thineJourneysEnd)
				{
					String thineLetterOfIndex = theCodeOfMorse.substring(0, thineIndex);
					Set<String> thineWordsTryToSpeaketh = myDictionary.getWordsForCode(thineLetterOfIndex);

					if(thineWordsTryToSpeaketh != null)
					{
						String leftOverEnglish = theCodeOfMorse.substring(thineIndex); //Performance hack.

						for(String stringOfStrings : thineWordsTryToSpeaketh)
						{
							String newEnglish = thineGargledSentence + stringOfStrings + " ";
							myDecryptingMachine(newEnglish, leftOverEnglish);
						}
					}

					thineIndex++;
				}
			}
		}

	}

	public float checkThineCoherence(String thineLessGargledMessage)
	{
		String [] seperateThineGargledMess = thineLessGargledMessage.split(" ");
		float thineFrequency = 1.0f;
		String thineFavoredLastWord = "";

		for(String stringOfStrings : seperateThineGargledMess)
		{
			if(thineFavoredLastWord.isEmpty())
			{
				thineFavoredLastWord = stringOfStrings;
			}
			else
			{
				thineFrequency *= (myDictionary.frequencyOfFollowingWord(thineFavoredLastWord, stringOfStrings) / DIVIDE_FREQUENCY);
				thineFavoredLastWord = stringOfStrings;
			}
		}
		return thineFrequency;
	}

	public List<String> myTranslationBuffer()
	{
		if(myGuessedTranslations.size() != 0)
		{
			//Puts the most recent items first in the order.
			Collections.sort(myGuessedTranslations);
			myTranslation = new ArrayList<String>();

			//Gets the top permutations, if there is more than the listed amount, if there is less, it will print out the list as a whole instead.
			for(int i = 0; i < TOP_PERMUTATIONS && i < myGuessedTranslations.size(); i++)
			{
				myTranslation.add(myGuessedTranslations.get(i).toString());
			}
		}
		return myTranslation;

	}








}

